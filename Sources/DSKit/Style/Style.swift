//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import Foundation
import UIKit

public struct Style {
    
    public enum Font {
        //new
        case titleBoldCustom(value: CGFloat)
        case regularCustom(value: CGFloat)
        case blackCustom(value: CGFloat)
        case blackItalicCustom(value: CGFloat)
        case boldItalicCustom(value: CGFloat)
        case italicCustom(value: CGFloat)
        case lightCustom(value: CGFloat)
        case lightItalicCustom(value: CGFloat)
        case mediumCustom(value: CGFloat)
        case normalCustom(value: CGFloat)
        case mediumItalicCustom(value: CGFloat)

        public var font: UIFont {
            switch self {
            // new
            case .titleBoldCustom(let value): return UIFont.init(name: "Rubik-Bold", size: value) ?? UIFont.systemFont(ofSize: value)
            case .regularCustom(let value): return UIFont.init(name: "Rubik-Regular", size: value) ?? UIFont.systemFont(ofSize: value)
            case .blackCustom(let value): return UIFont.init(name: "Rubik-Black", size: value) ?? UIFont.systemFont(ofSize: value)
            case .blackItalicCustom(let value): return UIFont.init(name: "Rubik-BlackItalic", size: value) ?? UIFont.systemFont(ofSize: value)
            case .boldItalicCustom(let value): return UIFont.init(name: "Rubik-BoldItalic", size: value) ?? UIFont.systemFont(ofSize: value)
            case .italicCustom(let value): return UIFont.init(name: "Rubik-Italic", size: value) ?? UIFont.systemFont(ofSize: value)
            case .lightCustom(let value): return UIFont.init(name: "Rubik-Light", size: value) ?? UIFont.systemFont(ofSize: value)
            case .lightItalicCustom(let value): return UIFont.init(name: "Rubik-LightItalic", size: value) ?? UIFont.systemFont(ofSize: value)
            case .mediumCustom(let value): return UIFont.init(name: "Rubik-Medium", size: value) ?? UIFont.systemFont(ofSize: value)
            case .mediumItalicCustom(let value): return UIFont.init(name: "Rubik-MediumItalic", size: value) ?? UIFont.systemFont(ofSize: value)
             
            case .normalCustom(let value): return UIFont.systemFont(ofSize: value)

            }
        }
    }
}
