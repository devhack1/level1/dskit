//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit

// MARK: - ThemeAttributes
public extension UILabel {
    private enum ThemeAttributeName: String {
        case attributeTextColor
        case attributeForegroundTextColor
    }
    
    var attributeTextColor: ColorAttribute? {
        get {
            return getThemeAttribute(by: ThemeAttributeName.attributeTextColor.rawValue) as! ColorAttribute?
        } set {
            setThemeAttribute(newValue, propertyName: ThemeAttributeName.attributeTextColor.rawValue) { [weak self] color in
                guard let color = color as? ColorAttribute else { return }
                self?.textColor = ThemeManager.shared.currentTheme.getColor(color)
            }
        }
    }
    
    var attributeForegroundTextColor: ColorAttribute? {
        get {
            return getThemeAttribute(by: ThemeAttributeName.attributeForegroundTextColor.rawValue) as? ColorAttribute
        } set {
            setThemeAttribute(
                newValue,
                propertyName: ThemeAttributeName.attributeForegroundTextColor.rawValue)
            { [weak self] color in
                guard let color = color as? ColorAttribute else { return }
                let attributedText = self?.attributedText ?? NSAttributedString(string: self?.text ?? "")
                let mutableAttributedText = NSMutableAttributedString(attributedString: attributedText)
                mutableAttributedText.addAttribute(
                    NSAttributedString.Key.foregroundColor,
                    value: color,
                    range: NSRange(location: 0, length: attributedText.length))
                self?.attributedText = mutableAttributedText
            }
        }
    }
}
