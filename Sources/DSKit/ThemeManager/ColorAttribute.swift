//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import Foundation

public enum ColorAttribute: String, CaseIterable {
    case primary
    case secondary
    case textPrimary
    case backgroundPrimary
    case linkAccent
    case bannerBackground
    case primaryBlue
    case secondaryBlue
    case primaryGray
    case primaryHeader
}
