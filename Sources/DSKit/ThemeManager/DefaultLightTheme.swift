//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit

public class DefaultLightTheme: NSObject, ThemeProtocol {
    public var name: String = ThemeManager.Theme.defaultLight.rawValue
    
    private(set) public var colors: [ColorAttribute: UIColor] = [
        .primary: ColorSet.greenLight.value,
        .secondary: ColorSet.yellowLight.value,
        .textPrimary: ColorSet.textLight.value,
        .backgroundPrimary: ColorSet.backgroundLight.value,
        .primaryBlue: ColorSet.primarybluegrayLight.value,
        .secondaryBlue: ColorSet.secondarybluegrayLight.value,
        .primaryGray: ColorSet.primaryGrayLight.value,
        .linkAccent: ColorSet.linkAccentLight.value,
        .bannerBackground: ColorSet.bannerBackgroundLight.value,
        .primaryHeader: ColorSet.primaryHeaderLight.value
    ]
}
