//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit


public enum ColorSet: Int, CaseIterable {
    case greenLight
    case greenDark
    
    case yellowLight
    case yellowDark
    
    case textLight
    case textDark

    case backgroundLight
    case backgrounDark

    case linkAccentLight
    case linkAccentDark

    case bannerBackgroundLight
    case bannerBackgroundDark
    
    case primarybluegrayLight
    case primarybluegrayDark
    
    case secondarybluegrayLight
    case secondarybluegrayDark
    
    case primaryGrayLight
    case primaryGrayDark
    
    case primaryHeaderLight
    case primaryHeaderDark
    
    public var value: UIColor {
        switch self {
        case .greenLight: return UIColor(red: 0.961, green: 0.961, blue: 0.969, alpha: 1)
        case .greenDark: return UIColor(red: 0.294, green: 0.302, blue: 0.349, alpha: 1)
            
        case .yellowLight: return UIColor(red: 1, green: 0.8, blue: 0, alpha: 1)
        case .yellowDark: return UIColor(red: 1, green: 0.839, blue: 0.039, alpha: 1)
            
        case .textLight: return UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        case .textDark: return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            
        case .backgroundLight: return UIColor(red: 0.949, green: 0.949, blue: 0.969, alpha: 1)
        case .backgrounDark: return UIColor(red: 0.11, green: 0.11, blue: 0.118, alpha: 1)
        
        case .primarybluegrayLight: return UIColor(red: 0.161, green: 0.298, blue: 0.396, alpha: 1)
        case .primarybluegrayDark: return UIColor(red: 0.961, green: 0.965, blue: 0.973, alpha: 1)
        
        case .secondarybluegrayDark: return UIColor(red: 0.961, green: 0.965, blue: 0.973, alpha: 1)
        case .secondarybluegrayLight: return UIColor(red: 0.529, green: 0.608, blue: 0.663, alpha: 1)
            
        case .primaryGrayLight: return UIColor(red: 0.965, green: 0.965, blue: 0.965, alpha: 1)
        case .primaryGrayDark: return UIColor(red: 0.475, green: 0.475, blue: 0.475, alpha: 1)
            
        case .linkAccentLight: return UIColor(red: 0.962, green: 0.482, blue: 0.297, alpha: 1)
        case .linkAccentDark: return UIColor(red: 0.788, green: 0.359, blue: 0.194, alpha: 1)
            
        case .bannerBackgroundLight: return UIColor(red: 0.983, green: 0.897, blue: 0.908, alpha: 1)
        case .bannerBackgroundDark: return UIColor(red: 0.247, green: 0.165, blue: 0.165, alpha: 1)
            
        case .primaryHeaderLight: return UIColor(red: 0.871, green: 0.725, blue: 1, alpha: 1)
        case .primaryHeaderDark: return UIColor(red: 0.361, green: 0, blue: 0.698, alpha: 1)

        }
    }
    
    
}
