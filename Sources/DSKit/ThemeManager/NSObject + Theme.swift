//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import Foundation

public extension NSObject {
    
    private typealias ThemeAttributes = [String: ThemeAttribute]
    
    private struct AssociatedKeys {
        static var themePropertiesKey = "themeAttributesKey"
    }
    
    private struct ThemeAttribute {
        var attribute: Any?
        var updateClosure: (Any?) -> Void
    }
    
    private var properties: ThemeAttributes {
        get {
            if let themeProperties = getThemAttributes() {
                return themeProperties
            } else {
                setThemeAttributes(ThemeAttributes())
                return getThemAttributes()!
            }
        } set {
            observeThemeUpdate()
            setThemeAttributes(newValue)
        }
    }
}

public extension NSObject {
    
    func setThemeAttribute(_ attribute: Any?, propertyName: String, updateClosure: @escaping (Any?) -> Void) {
        properties[propertyName] = ThemeAttribute(attribute: attribute, updateClosure: updateClosure)
        updateClosure(attribute)
    }
    
    func getThemeAttribute(by propertyName: String) -> Any? {
        return properties[propertyName]?.attribute
    }
}

extension NSObject: ThemeObserver {
    
    public func updateTheme() {
        properties.forEach { $0.value.updateClosure($0.value.attribute) }
    }
}

private extension NSObject {
    
    private func setThemeAttributes(_ attributes: ThemeAttributes) {
        objc_setAssociatedObject(self, &AssociatedKeys.themePropertiesKey, attributes, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    private func getThemAttributes() -> ThemeAttributes? {
        return objc_getAssociatedObject(self, &AssociatedKeys.themePropertiesKey) as? ThemeAttributes
    }
    
    private func observeThemeUpdate() {
        if properties.isEmpty { ThemeManager.shared.addObserver(self) }
    }
}


public extension Notification.Name {
    static let themeUpdated = Notification.Name("themeUpdatedNotificatioinName")
}
