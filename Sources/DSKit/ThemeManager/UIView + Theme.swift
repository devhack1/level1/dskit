//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit

public extension UIView {
    enum ThemeAttributeName: String {
        case attributeTintColor
        case attributeBorderColor
        case attributeBackgroundColor
    }
    
    var attributeBackgroundColor: ColorAttribute? {
        get {
            getThemeAttribute(by: ThemeAttributeName.attributeBackgroundColor.rawValue) as? ColorAttribute
        }
        set {
            setThemeAttribute(newValue, propertyName: ThemeAttributeName.attributeBackgroundColor.rawValue) { [weak self] attribute in
                guard let attribute = attribute as? ColorAttribute else { return }
                self?.backgroundColor = ThemeManager.shared.currentTheme.getColor(attribute)
            }
        }
    }
    
    var attributeBorderColor: ColorAttribute? {
        get {
            return getThemeAttribute(by: ThemeAttributeName.attributeBorderColor.rawValue) as? ColorAttribute
        } set {
            setThemeAttribute(newValue, propertyName: ThemeAttributeName.attributeBorderColor.rawValue) { [weak self] attribute in
                guard let attribute = attribute as? ColorAttribute else { return }
                self?.layer.borderColor = ThemeManager.shared.currentTheme.getColor(attribute)?.cgColor
            }
        }
    }
    
    var attributeTintColor: ColorAttribute? {
        get {
            return getThemeAttribute(by: ThemeAttributeName.attributeTintColor.rawValue) as? ColorAttribute
        } set {
            setThemeAttribute(newValue, propertyName: ThemeAttributeName.attributeTintColor.rawValue) { [weak self] attribute in
                guard let attribute = attribute as? ColorAttribute else { return }
                self?.tintColor = ThemeManager.shared.currentTheme.getColor(attribute)
            }
        }
    }
    
    // MARK: - Functions
    func setAttributeBackgroundColor(_ attribute: ColorAttribute, opacity: CGFloat) {
        
    }
}
