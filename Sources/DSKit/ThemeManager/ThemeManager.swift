//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import Foundation
import UIKit

@objcMembers
public class ThemeManager: NSObject {
    
    public enum Theme: String, CaseIterable {
        case `default`
        case defaultDark
        case defaultLight
        var value: ThemeProtocol {
            switch self {
            case .default: return DefaultLightTheme()
            case .defaultDark: return DefaultDarkTheme()
            case .defaultLight: return DefaultLightTheme()
            }
        }
    }
    
    private let themeKey = "applicationTheme"
    private(set) public var currentTheme = Theme.default.value
    
    private var observersStorage = [WeakObject<ThemeObserver>]()
    private var observers: [ThemeObserver] {
        get { getObservers() }
        set { saveObservers(newValue) }
    }
    
    public static let shared = ThemeManager()
    
    private override init() {
        super.init()
        self.currentTheme = getApplicationTheme()
        self.saveApplicationTheme()
        self.updateApplicationTheme()
    }
}

public extension ThemeManager {

    func setApplicationTheme(_ theme: ThemeManager.Theme) {
        self.currentTheme = theme.value
        saveApplicationTheme()
        updateApplicationTheme()
    }
    func setApplicationTheme(_ userInterfaceStyle: UIUserInterfaceStyle) {
        if userInterfaceStyle == .dark {
            self.currentTheme = ThemeManager.Theme.defaultDark.value
        } else {
            self.currentTheme = ThemeManager.Theme.defaultLight.value
        }
        saveApplicationTheme()
        updateApplicationTheme()
    }
    
    func addObserver(_ observer: ThemeObserver) {
        observers.append(observer)
    }
}

private extension ThemeManager {
    
    private func getObservers() -> [ThemeObserver] {
        observersStorage.compactMap { $0.value }
    }
    
    private func saveObservers(_ observers: [ThemeObserver]) {
        observersStorage = observers.map { WeakObject($0) }
    }
    
    private func saveApplicationTheme() {
        UserDefaults.standard.set(currentTheme.name, forKey: themeKey)
    }
    
    private func updateApplicationTheme() {
        observers.forEach { $0.updateTheme() }
    }

    private func getApplicationTheme() -> ThemeProtocol {
        guard let themeName = UserDefaults.standard.string(forKey: themeKey) else { return Theme.default.value }
        return Theme.allCases.first(where: { $0.rawValue == themeName ? true : false })?.value ?? Theme.default.value
    }
}

public class WeakObject<T: AnyObject> {
    public weak var value: T?
    init(_ value: T) { self.value = value }
}

@objc
public protocol ThemeObserver: AnyObject {
    func updateTheme()
}

public protocol ThemeProtocol {
    var name: String { get }
    var colors: [ColorAttribute: UIColor] { get }
}

public extension ThemeProtocol {
    
    func getColor(_ color: ColorAttribute) -> UIColor? {
        return colors[color]
    }
}
