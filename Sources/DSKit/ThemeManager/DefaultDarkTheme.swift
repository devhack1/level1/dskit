//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit

@objcMembers
public class DefaultDarkTheme: NSObject, ThemeProtocol {
    
    public var name: String = ThemeManager.Theme.defaultDark.rawValue
    
    private(set) public var colors: [ColorAttribute: UIColor] = [
        .primary: ColorSet.greenDark.value,
        .secondary: ColorSet.yellowDark.value,
        .textPrimary: ColorSet.textDark.value,
        .backgroundPrimary: ColorSet.backgrounDark.value,
        .primaryBlue: ColorSet.primarybluegrayDark.value,
        .secondaryBlue: ColorSet.secondarybluegrayDark.value,
        .primaryGray: ColorSet.primaryGrayDark.value,
        .linkAccent: ColorSet.linkAccentDark.value,
        .bannerBackground: ColorSet.bannerBackgroundDark.value,
        .primaryHeader: ColorSet.primaryHeaderDark.value
    ]
}
