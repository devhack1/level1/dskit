//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit

public extension UIFont {
    static let caption1 = UIFont.systemFont(ofSize: 11, weight: .regular)
    static let body1 = UIFont.systemFont(ofSize: 16, weight: .regular)
    static let body2 = UIFont.systemFont(ofSize: 16, weight: .semibold)
}
