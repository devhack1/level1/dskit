//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import Foundation
import UIKit
import SnapKit
import KMPCore

//public
//protocol BaseMainRowButtonDelegate: AnyObject {
//    func didChangeButtonState(completion: @escaping () -> Void)
//}

public
class BaseMainRowButton: UIButton {
    
    // MARK: Public properties
    
    public enum State {
        case up
        case down
    }
    
    // MARK: Private properties
    
    private let imageUp = UIImage(named: "plainUp", in: Bundle.module, compatibleWith: nil)
    
    private let imageDown = UIImage(named: "plainDown", in: Bundle.module, compatibleWith: nil)
    
    private var buttonState: State = .up
    
    private var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    public var image: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    // MARK: Init

    public init(state: State, name: String) {
        self.buttonState = state
        super.init(frame: .zero)
        backgroundColor = .white
        self.layer.cornerRadius = 20
        
        configureButton()
        let color = ThemeManager.shared.currentTheme.getColor(.secondaryBlue) ?? .black
        label.attributedText = name
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 24).font)
            .color(color)
        switch buttonState {
        case .up:
            image.image = imageUp
            break
        case .down:
            image.image = imageDown
            break
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureButton() {
        self.addSubview(label)
        self.addSubview(image)
        
        label.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(CGFloat.leftInset)
        }
        
        image.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalTo(-CGFloat.leftInset)
            make.width.height.equalTo(14)
        }
    }
    
    // MARK: Override
    
    public func buttonAction() {
        switch buttonState {
        case .up:
            image.image = imageDown
            buttonState = .down
        case .down:
            image.image = imageUp
            buttonState = .up
        }
    }
}
