//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import Foundation
import UIKit
import SnapKit

public
class BaseMainCountCell: UITableViewCell {
    
    private var firstLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var secondLabel: BasePrimaryLabel = {
        let label = BasePrimaryLabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.attributeBackgroundColor = ColorAttribute.secondary
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
        return label
    }()
    
    private var thirdLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private var imageCard: UIImageView = {
        let imageVIew = UIImageView()
        imageVIew.contentMode = .scaleAspectFit
        imageVIew.layer.cornerRadius = 4
        imageVIew.clipsToBounds = true
        return imageVIew
    }()
    private let imgContainer = UIView()

    private let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.axis = .horizontal
        stack.spacing = 8
        return stack
    }()
    private let smallStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .leading
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = 4
        return stack
    }()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
        selectionStyle = .none
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        firstLabel.attributedText = nil
        secondLabel.attributedText = nil
        thirdLabel.attributedText = nil
    }
    
    public func configire(
        image: UIImage? = nil,
        firstText: NSAttributedString? = nil,
        secondText: NSAttributedString? = nil,
        thirdText: NSAttributedString? = nil
    ) {
        imageCard.image = image
        imgContainer.isHidden = image == nil
        firstLabel.attributedText = firstText
        secondLabel.attributedText = secondText
        thirdLabel.attributedText = thirdText
    }
    
    private func commonInit() {
        contentView.attributeBackgroundColor = ColorAttribute.backgroundPrimary
        self.backgroundView?.attributeBackgroundColor = ColorAttribute.backgroundPrimary
        imgContainer.addSubview(imageCard)
        smallStack.addArrangedSubview(firstLabel)
        smallStack.addArrangedSubview(secondLabel)
        smallStack.addArrangedSubview(thirdLabel)
        mainStack.addArrangedSubview(imgContainer)
        mainStack.addArrangedSubview(smallStack)
        contentView.addSubview(mainStack)
        mainStack.snp.makeConstraints {
            let inset = UIEdgeInsets(top: 8, left: .leftInset, bottom: 8, right: .rightInset)
            $0.edges.equalToSuperview().inset(inset)
        }
        imageCard.snp.makeConstraints { make in
            make.top.greaterThanOrEqualToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview().priority(.high)
            make.centerY.equalToSuperview()
            make.height.equalTo(50)
            make.width.equalTo(80)
        }
    }
    
}
