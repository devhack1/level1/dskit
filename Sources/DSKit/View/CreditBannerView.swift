//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import UIKit
import SnapKit

public class CreditBannerView: UIView {

    public let label = UILabel()
    public let button = UIButton(type: .system)

    public init() {
        super.init(frame: .zero)
        setColors()
        addSubview(label)
        addSubview(button)
        label.numberOfLines = 0
        label.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(12)
        }
        button.snp.makeConstraints {
            $0.top.equalTo(label.snp.bottom).offset(12)
            $0.leading.trailing.bottom.equalToSuperview().inset(12)
        }
        layer.cornerRadius = 12
        clipsToBounds = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var currentTheme: ThemeProtocol { ThemeManager.shared.currentTheme }

    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
        setColors()
    }

    func setColors() {
        label.textColor = currentTheme.getColor(.textPrimary)
        button.setTitleColor(currentTheme.getColor(.linkAccent), for: .normal)
        backgroundColor = currentTheme.getColor(.bannerBackground)
    }

}
