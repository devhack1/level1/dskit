//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit

public extension CGFloat {
    static let leftInset: CGFloat = 24
    static let rightInset: CGFloat = 24
}
