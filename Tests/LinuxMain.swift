import XCTest

import DSKitTests

var tests = [XCTestCaseEntry]()
tests += DSKitTests.allTests()
XCTMain(tests)
